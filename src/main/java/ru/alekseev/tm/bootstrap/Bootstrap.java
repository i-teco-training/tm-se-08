package ru.alekseev.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import ru.alekseev.tm.api.*;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.entity.RoleType;
import ru.alekseev.tm.repository.ProjectRepository;
import ru.alekseev.tm.repository.TaskRepository;
import ru.alekseev.tm.repository.UserRepository;
import ru.alekseev.tm.service.ProjectService;
import ru.alekseev.tm.service.TaskService;
import ru.alekseev.tm.service.TerminalService;
import ru.alekseev.tm.service.UserService;
import ru.alekseev.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class Bootstrap implements ServiceLocator {
    @NotNull private final IProjectRepository projectRepository = new ProjectRepository();
    @NotNull private final IProjectService projectService = new ProjectService(projectRepository);
    @NotNull private final ITaskRepository taskRepository = new TaskRepository();
    @NotNull private final ITaskService taskService = new TaskService(taskRepository);
    @NotNull private final IUserRepository userRepository = new UserRepository();
    @NotNull private final IUserService userService = new UserService(userRepository);
    @NotNull private final TerminalService terminalService = new TerminalService();

    @NotNull private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    @NotNull
    public final List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    @Override
    @NotNull
    public final IProjectService getProjectService() {
        return projectService;
    }

    @Override
    @NotNull
    public final ITaskService getTaskService() {
        return taskService;
    }

    @Override
    @NotNull
    public final IUserService getUserService() {
        return userService;
    }

    @Override
    @NotNull
    public final TerminalService getTerminalServise() {
        return terminalService;
    }

    public final void registry(@NotNull final AbstractCommand command) {
        @NotNull String commandName = command.getName();
        commands.put(commandName, command);
    }

    public final void start(@NotNull final Class[] CLASSES) throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");

        initCommands(CLASSES);
        initUsers();

        while (true) {
            @NotNull String commandName = terminalService.getFromConsole();
            if (!commands.containsKey(commandName)) {
                commandName = "help";
            }

            if (commands.get(commandName).isSecure() && !this.userService.isAuthorized()) {
                System.out.println("LOGIN INTO PROJECT MANAGER");
                System.out.println();
                continue;
            }

            commands.get(commandName).execute();
            System.out.println();
        }
    }

    public final void initCommands(@NotNull final Class[] CLASSES) throws Exception {
        for (Class commandClass : CLASSES) {
            if (AbstractCommand.class.isAssignableFrom(commandClass)) {
                AbstractCommand command = (AbstractCommand) commandClass.newInstance();
                command.setServiceLocator(this);
                registry(command);
            }
        }
    }

    public final void initUsers() {
        this.userService.addByLoginPasswordUserRole("user1", HashUtil.getMd5("www"), RoleType.admin);
        this.userService.addByLoginPasswordUserRole("user2", HashUtil.getMd5("www"), RoleType.user);
    }
}