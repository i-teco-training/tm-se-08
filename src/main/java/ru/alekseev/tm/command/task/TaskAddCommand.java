package ru.alekseev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.entity.Task;

public final class TaskAddCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "add-task";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Add new task";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[ADDING OF NEW TASK]");
        System.out.println("TYPE \"1\" TO ATTACH NEW TASK TO EXISTING PROJECT (otherwise press \"ENTER\" key)");
        @NotNull final String userChoice = serviceLocator.getTerminalServise().getFromConsole();
        String projectId = null;
        if ("1".equals(userChoice)) {
            System.out.println("ENTER PROJECT ID");
            projectId = serviceLocator.getTerminalServise().getFromConsole();
        }
        System.out.println("ENTER TASK NAME");
        @NotNull final String newName = serviceLocator.getTerminalServise().getFromConsole();
        @NotNull final String currentUserId = serviceLocator.getUserService().getCurrentUser().getId();
        @NotNull final Task task = new Task();
        task.setUserId(currentUserId);
        task.setProjectId(projectId);
        task.setName(newName);
        serviceLocator.getTaskService().add(task);
        System.out.println("[OK]");
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}
