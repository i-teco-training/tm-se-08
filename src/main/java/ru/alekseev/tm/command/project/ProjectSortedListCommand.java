package ru.alekseev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.entity.Project;
import ru.alekseev.tm.entity.User;

import java.util.Comparator;
import java.util.List;

public class ProjectSortedListCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "sort-projects";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Show sorted list of all projects";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[SORTED LIST OF ALL PROJECTS]");
        @NotNull final User currentUser = serviceLocator.getUserService().getCurrentUser();
        @Nullable final List<Project> listForSorting = serviceLocator.getProjectService().findAllByUserId(currentUser.getId());
        if (listForSorting.size() == 0) {
            System.out.println("LIST OF PROJECTS IS EMPTY");
            return;
        }
        System.out.println("sort by?");
        System.out.println("1 - by date of creation");
        System.out.println("2 - by start");
        System.out.println("3 - by finish");
        System.out.println("4 - by status (planned, in-progress, ready)");
        System.out.println("ENTER 1, 2, 3 or 4");
        String input = serviceLocator.getTerminalServise().getFromConsole();
        switch (input) {
            case "1": listForSorting.sort(Comparator.comparing(Project::getCreatedOn));
            case "2": listForSorting.sort(Comparator.comparing(Project::getDateStart));
            case "3": listForSorting.sort(Comparator.comparing(Project::getDateFinish));
            case "4": listForSorting.sort(Comparator.comparing(Project::getStatus));
            default:
                System.out.println("Incorrect input. Try again.");
                execute();
        }
        for (int i = 0; i < listForSorting.size(); i++) {
            System.out.print(i + 1);
            System.out.println(") name:" + listForSorting.get(i).getName()
                    + ", projectId: " + listForSorting.get(i).getId()
                    + ", date of creation: " + listForSorting.get(i).getCreatedOn()
                    + ", start date: " + listForSorting.get(i).getDateStart()
                    + ", finish date: " + listForSorting.get(i).getDateFinish()
                    + ", projectId: " + listForSorting.get(i).getStatus());
        }
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}
