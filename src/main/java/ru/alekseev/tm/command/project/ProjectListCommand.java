package ru.alekseev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.entity.Project;
import ru.alekseev.tm.entity.User;

import java.util.List;

public final class ProjectListCommand extends AbstractCommand {
    @Override
    @NotNull
    public final String getName() {
        return "show-projects";
    }

    @Override
    @NotNull
    public final String getDescription() {
        return "Show list of all projects";
    }

    @Override
    public final void execute() throws Exception {
        System.out.println("[LIST OF ALL PROJECTS]");
        @NotNull final User currentUser = serviceLocator.getUserService().getCurrentUser();

        @Nullable final List<Project> list = serviceLocator.getProjectService().findAllByUserId(currentUser.getId());
        if (list.size() == 0) {
            System.out.println("LIST OF PROJECTS IS EMPTY");
        }
        for (int i = 0; i < list.size(); i++) {
            System.out.print(i + 1);
            System.out.println(") name:" + list.get(i).getName() + ", projectId: " + list.get(i).getId());
        }
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}
