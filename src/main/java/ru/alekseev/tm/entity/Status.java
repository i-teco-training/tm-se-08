package ru.alekseev.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@Getter
@NoArgsConstructor
public enum Status {
    planned("запланировано"),
    inProgress("в процессе"),
    ready("готово");

    private String displayName;

    Status(final String displayName) {
        this.displayName = displayName;
    }


    @Override
    @Nullable
    public final String toString() {
        return this.displayName;
    }
}
