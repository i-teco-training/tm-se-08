package ru.alekseev.tm;

import org.jetbrains.annotations.NotNull;
import ru.alekseev.tm.bootstrap.Bootstrap;
import ru.alekseev.tm.command.project.*;
import ru.alekseev.tm.command.system.AboutCommand;
import ru.alekseev.tm.command.system.ExitCommand;
import ru.alekseev.tm.command.system.HelpCommand;
import ru.alekseev.tm.command.task.TaskAddCommand;
import ru.alekseev.tm.command.task.TaskDeleteCommand;
import ru.alekseev.tm.command.task.TaskListCommand;
import ru.alekseev.tm.command.task.TaskUpdateCommand;
import ru.alekseev.tm.command.user.*;

public class Application {
    private static final Class[] CLASSES = {
            ProjectSortedListCommand.class,
            ProjectAddCommand.class, ProjectDeleteCommand.class,
            ProjectListCommand.class, ProjectUpdateCommand.class,
            AboutCommand.class, ExitCommand.class, HelpCommand.class,
            TaskAddCommand.class, TaskDeleteCommand.class,
            TaskListCommand.class, TaskUpdateCommand.class,
            UserEditCurrentProfileCommand.class, UserListCommand.class,
            UserLoadCurrentProfileCommand.class, UserLoginCommand.class,
            UserLogoutCommand.class, UserPasswordUpdateCommand.class, UserSignupCommand.class
    };

    public static void main(String[] args) throws Exception {
        @NotNull Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(CLASSES);
    }
}
