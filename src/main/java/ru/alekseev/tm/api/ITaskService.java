package ru.alekseev.tm.api;

import ru.alekseev.tm.entity.Task;

import java.util.List;

public interface ITaskService extends IService<Task>{

    List<Task> findAllByUserId(String userId);

    void updateByNewData(String userId, String taskId, String name);

    void deleteByUserIdAndTaskId(String userId, String taskId);

    void clearByProjectId(String projectId);
}